// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/RPGCharacterBase.h"
#include "RPGEnemyCharacter.generated.h"

/**
 * 
 */
UCLASS()
class AURA_API ARPGEnemyCharacter : public ARPGCharacterBase
{
	GENERATED_BODY()
	
public:
	ARPGEnemyCharacter();
};
